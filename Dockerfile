FROM node:lts-alpine
LABEL authors="Edward Salter"

ENV NODE_ENV=production

WORKDIR /app

COPY ["package.json", "package-lock.json*", "index.js", "./"]

RUN npm install --omit=dev

ENV PORT=9548
EXPOSE 9548

CMD ["npm", "run", "start:prod"]
