const Docker = require("dockerode");
const express = require("express");
const sumBy = require("lodash/sumBy");
const client = require("prom-client");

// Load the docker API. Will automatically use DOCKER_HOST and DOCKER_CERT_PATH from the environment if defined.
const docker = new Docker();

const statusGauge = new client.Gauge({
  name: "container_status",
  help: "Status of a container",
  labelNames: ["container", "status", "health"],
});
const memoryGauge = new client.Gauge({
  name: "container_memory_usage",
  help: "Memory usage of a container in bytes",
  labelNames: ["container", "status", "health"],
});
const memoryLimitGauge = new client.Gauge({
  name: "container_memory_limit",
  help: "Maximum amount of memory that container is allowed to consume in bytes",
  labelNames: ["container", "status", "health"],
});
const cpuGauge = new client.Gauge({
  name: "container_cpu_usage",
  help: "Amount of CPU used by the container as a percentage",
  labelNames: ["container", "status", "health"],
});
const txGauge = new client.Gauge({
  name: "container_network_transmit_bytes",
  help: "Amount of network traffic transmitted by the container in bytes",
  labelNames: ["container", "status", "health"],
});
const rxGauge = new client.Gauge({
  name: "container_network_receive_bytes",
  help: "Amount of network traffic received by the container in bytes",
  labelNames: ["container", "status", "health"],
});

async function getStats(containerId) {
  const container = docker.getContainer(containerId);

  const statsPromise = container.stats({ stream: false });
  const infoPromise = container.inspect();
  const [stats, info] = await Promise.all([statsPromise, infoPromise]);

  const networks = stats.networks && Object.values(stats.networks);

  const cpuDelta =
    stats.cpu_stats.cpu_usage.total_usage -
    stats.precpu_stats.cpu_usage.total_usage;
  const systemDelta =
    stats.cpu_stats.system_cpu_usage - stats.precpu_stats.system_cpu_usage;
  const cpuPercent =
    (cpuDelta / systemDelta) * stats.cpu_stats.online_cpus * 100;

  return {
    id: containerId,
    name: stats.name.replace(/^\//, ""),
    status: info.State?.Status,
    health: info.State?.Health?.Status,
    network: {
      tx: networks && sumBy(networks, "tx_bytes"),
      rx: networks && sumBy(networks, "rx_bytes"),
    },
    memory: {
      usage: stats.memory_stats.usage,
      limit: stats.memory_stats.limit,
    },
    cpu: cpuPercent,
  };
}

async function getAllStats() {
  const containers = await docker.listContainers({
    all: true,
  });
  const containerIds = containers.map((container) => container.Id);
  return Promise.all(containerIds.map(getStats));
}

const app = express();
app.get("/", (req, res) => {
  res.contentType("text/html");
  res.send(
    '<html><body><h1>Container Metrics Exporter</h1><a href="/metrics">Metrics</a></body></html>'
  );
});

app.use("/metrics", async (req, res) => {
  try {
    client.register.resetMetrics();

    const stats = await getAllStats();

    stats.forEach((stat) => {
      const statusLabels = {
        container: stat.name,
        status: stat.status,
        health: stat.health,
      };
      if (stat.status !== "running" || stat.health == null) {
        delete statusLabels.health;
      }

      statusGauge.set(statusLabels, stat.status === "running" ? 1 : 0);
      memoryGauge.set(statusLabels, getValue(stat.memory.usage));
      memoryLimitGauge.set(statusLabels, getValue(stat.memory.limit));
      cpuGauge.set(statusLabels, getValue(stat.cpu));
      rxGauge.set(statusLabels, getValue(stat.network.rx));
      txGauge.set(statusLabels, getValue(stat.network.tx));
    });

    const metrics = await client.register.metrics();
    res.contentType(client.register.contentType);
    res.send(metrics);
  } catch (err) {
    console.error(err);
    res.status(500).send(err.message);
  }
});

function getValue(value) {
  return Number.isFinite(value) ? value : 0;
}

const listenPort = process.env.PORT || 9548;
app.listen(listenPort, () => {
  console.log("Listening on port", listenPort);
});
